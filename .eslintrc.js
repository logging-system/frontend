module.exports = {
  env: {
    browser: true,
    es2021:  true
  },
  extends: [
    'react-app',
    'react-app/jest',
    'eslint:recommended',
  ],
  parser:        '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType:  'module',
  },
  plugins: [
    '@typescript-eslint',
    'eslint-plugin-prettier',
    'simple-import-sort'
  ],
  globals: {
    JSX:    true,
    NodeJS: true,
  },
  rules: {
    '@typescript-eslint/no-unused-vars': ['error'],
    'semi':                              [2, 'always'],
    'quotes':                            [2, 'single', 'avoid-escape'],
    'indent':                            [1, 2, {
      'ImportDeclaration': 'first',
    }],
    'object-curly-spacing': [2, 'always', {
      'objectsInObjects': false
    }],
    'key-spacing': [2, {
      align: 'value'
    }],
    'react-hooks/exhaustive-deps':               0,
    '@typescript-eslint/member-delimiter-style': ['warn', {
      'multiline': {
        'delimiter':   'comma',
        'requireLast': true
      },
      'singleline': {
        'delimiter':   'comma',
        'requireLast': true
      },
      'overrides': {
        'interface': {
          'multiline': {
            'delimiter':   'semi',
            'requireLast': true
          }
        }
      },
    }],
    'simple-import-sort/exports': 'error',
    'simple-import-sort/imports': 'error',
    'no-case-declarations':       0,
  }
};
