
export enum LogLevel {
    trace = 1,
    debug = 2,
    info = 3,
    warn = 4,
    error = 5,
    fatal = 6,
}

export type LogLevelOption = {
    name: string,
    value: LogLevel,
}

export const LogLevelOptions: LogLevelOption[] = [
  {
    name:  'trace',
    value: 1,
  },
  {
    name:  'debug',
    value: 2,
  },
  {
    name:  'info',
    value: 3,
  },
  {
    name:  'warn',
    value: 4,
  },
  {
    name:  'error',
    value: 5,
  },
  {
    name:  'fatal',
    value: 6,
  }
];

