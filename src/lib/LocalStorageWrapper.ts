export class LocalStorageWrapper {
  public add<T>(key: string, data: Record<string, T>) {
    return window.localStorage.setItem(key, JSON.stringify(data));
  }

  public get<T>(key: string): Record<string, T> | null {
    const rawData = window.localStorage.getItem(key);

    if (!rawData) {
      return null;
    }

    return JSON.parse(rawData);
  }

  public clear() {
    return window.localStorage.clear();
  }

  public delete(key:  string) {
    return window.localStorage.removeItem(key);
  }
}

const StorageHelper = new LocalStorageWrapper();
export default StorageHelper;