import { LogLevel } from '../../../LogLevels';

export type LogEntity = {
    id: string,
    level: LogLevel,
    message: string,
    service: string,
    timestamp: string,
    data: Record<string, any>,
}