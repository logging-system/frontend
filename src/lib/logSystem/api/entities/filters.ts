import { LogLevel } from '../../../LogLevels';

export type LoadLogsFilter = {
    page: number,
    limit: number,
    sortField: string,
    sortDirection: 'asc' | 'desc',
    collection: string,
    level: LogLevel,
    service: string,
    timeFrom: string,
    timeTo: string,
}