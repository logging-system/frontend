import { LoadLogsFilter } from './entities/filters';
import { LogEntity } from './entities/LogEntities';


const ENTRY_POINT = '/api';

export class LogSystemAPI {
  constructor(private accessKey: string){}

  async loadLogs(parameters: Partial<LoadLogsFilter>): Promise<LogEntity[]> {
    let url  = `${ENTRY_POINT}/log/load?`;
    
    for (const key in parameters) {
      const parameterName = key as keyof LoadLogsFilter;

      if (parameters[parameterName]){
        url += `${key}=${parameters[parameterName]}&`;
      }
    }

    const request = await fetch(url, {
      headers: {
        'Authorization': this.accessKey,
      }
    });

    const response = await request.json();

    return response.data ? response.data : [];
  }
}