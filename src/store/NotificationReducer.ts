import { AlertColor } from '@mui/material';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface NotificationProps {
  type: AlertColor;
  message: string;
}

export const NotificationReducer = createSlice({
  name:         'notification',
  initialState: {
    snackMessageType: 'info' as AlertColor,
    snackBarMessage:  '',
    notifications:    [] as NotificationProps[],
  },
  reducers: {
    addNotification: (state, action: PayloadAction<NotificationProps>) => {
      state.notifications = [...state.notifications, action.payload];
      state.snackBarMessage = action.payload.message;
      state.snackMessageType = action.payload.type;
    },
    setSnackMessage: (state, action: PayloadAction<{message: string, type: AlertColor,}>) => {
      state.snackBarMessage = action.payload.message;
      state.snackMessageType = action.payload.type;
    },
    deleteNotification: (state, action: PayloadAction<number>) => {
      state.notifications.splice(action.payload, 1);
    },
  },
});

// Action creators are generated for each case reducer function
export const { addNotification, setSnackMessage, deleteNotification } = NotificationReducer.actions;

export default NotificationReducer.reducer;
