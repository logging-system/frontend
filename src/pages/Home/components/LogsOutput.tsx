import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Paper from '@mui/material/Paper';
import Select from '@mui/material/Select';
import Stack from '@mui/material/Stack';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TextField from '@mui/material/TextField';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react';

import { LogLevel, LogLevelOptions } from '../../../lib/LogLevels';
import { LoadLogsFilter } from '../../../lib/logSystem/api/entities/filters';
import { LogEntity } from '../../../lib/logSystem/api/entities/LogEntities';
import { LogSystemAPI } from '../../../lib/logSystem/api/LogSystemApi';
import { LogRow } from './LogRow';

type ComponentProps = {
    logSystemKey: string,
}

export const LogsOutput: React.FC<ComponentProps> = (props) => {
  const [level, setLevel] = useState<LogLevel>();
  const [serviceName, setServiceName] = useState('');
  const [collection, setCollection] = useState('');
  const [timeFrom, setTimeFrom] = useState<dayjs.Dayjs>();
  const [timeTo, setTimeTo] = useState<dayjs.Dayjs>();

  const [currentPage, setCurrentPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(15);

  const [logs, setLogs] = useState<LogEntity[]>([]);

  const sendRequestToServer = async () => {
    const api = new LogSystemAPI(props.logSystemKey);

    const filters: Partial<LoadLogsFilter> = {
      page:       currentPage + 1,
      limit:      rowsPerPage,
      level:      level,
      service:    serviceName,
      collection: collection,
      timeFrom:   timeFrom?.toISOString(),
      timeTo:     timeTo?.toISOString(),
    };

    const result = await api.loadLogs(filters);
    setLogs(result);

  };

  const resetForm = () => {
    setLevel(undefined);
    setServiceName('');
    setCollection('');
    setTimeFrom(undefined);
    setTimeTo(undefined);
    setLogs([]);

    setTimeout(sendRequestToServer, 1000);
  };

  useEffect(() => {
    sendRequestToServer();
  }, [currentPage, rowsPerPage]);

  return (
    <React.Fragment>
      <Box
        component="form"
        noValidate
        autoComplete="off"
      >
        <Stack direction="row" columnGap={2}>
          <TextField type="text" value={serviceName} label="Service name" onChange={(event) => {
            setServiceName(event.target.value);
          }} />
          <FormControl>
            <InputLabel id="demo-simple-select-label">Level</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              autoWidth={false}
              value={level}
              label="Level"
              onChange={(event) => {
                const value = event.target.value as LogLevel;
                setLevel(value);
              }}
              style={{
                width: '79px',
              }}
            >
              {
                LogLevelOptions.map((option, index) => (
                  <MenuItem key={index} value={option.value}>{option.name}</MenuItem>
                ))
              }
            </Select>
          </FormControl>
          <TextField type="text" value={collection} label="Collection" onChange={(event) => {
            setCollection(event.target.value);
          }} />
          <DateTimePicker
            label="Time from"
            value={timeFrom ?? null}
            views={['year', 'month', 'day', 'hours', 'minutes', 'seconds']}
            onChange={(value: dayjs.Dayjs | null) => {
              setTimeFrom(value || undefined);
            }}
          />
          <DateTimePicker
            label="Time to"
            value={timeTo ?? null}
            views={['year', 'month', 'day', 'hours', 'minutes', 'seconds']}
            onChange={(value: dayjs.Dayjs | null) => {
              setTimeTo(value || undefined);
            }}
          />
          <Button type='button' variant="outlined" onClick={resetForm}>Reset</Button>
          <Button type='button' variant="contained" onClick={sendRequestToServer}>Apply</Button>
        </Stack>
      </Box>
      <Box
        marginTop="3rem"
        component="div">
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell></TableCell>
                <TableCell>Id</TableCell>
                <TableCell>Level</TableCell>
                <TableCell>Message</TableCell>
                <TableCell>Service</TableCell>
                <TableCell>Timestamp</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {logs.map((row) => (
                <LogRow row={row} />
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          count={-1}
          page={currentPage}
          onPageChange={(event, page) => {
            setCurrentPage(page);
          }}
          rowsPerPageOptions={
            [15, 30, 50, 100]
          }
          rowsPerPage={rowsPerPage}
          onRowsPerPageChange={(event) => {
            setRowsPerPage(parseInt(event.target.value, 10));
            setCurrentPage(0);
          }}
        />
      </Box>
    </React.Fragment>
  );
};