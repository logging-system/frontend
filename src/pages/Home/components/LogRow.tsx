import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import React from 'react';

import { LogEntity } from '../../../lib/logSystem/api/entities/LogEntities';

type ComponentProps = {
    row: LogEntity,
}

export const LogRow: React.FC<ComponentProps> = (props) => {
  const [open, setOpen] = React.useState(false);
  return (
    <React.Fragment>
      <TableRow
        key={props.row.id}
      >
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell>
          {props.row.id}
        </TableCell>
        <TableCell>
          {props.row.level}
        </TableCell>
        <TableCell>
          {props.row.message}
        </TableCell>
        <TableCell>
          {props.row.service}
        </TableCell>
        <TableCell>
          {props.row.timestamp}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <pre>
              {JSON.stringify(props.row.data, null, 2)}
            </pre>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
};