import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import React, { useEffect, useState } from 'react';

import StorageHelper from '../lib/LocalStorageWrapper';
import { LogsOutput } from './Home/components/LogsOutput';

const ACCESS_KEY_STORAGE = 'AccessKey';

export const HomePage: React.FC = () => {
  const [logSystemKey, setLogSystemKey] = useState('');
  const [isAppliedAccessKey, setAppliedAccessKey] = useState(false);

  useEffect(() => {
    const result = StorageHelper.get<string>(ACCESS_KEY_STORAGE);

    if (!result) {
      return;
    }

    setLogSystemKey(result.key);
    setAppliedAccessKey(true);

  }, []);

  return (
    <Container maxWidth="lg">
      {
        !isAppliedAccessKey && (
          <Box
            component="form"
            noValidate
            autoComplete="off"
          >
            <Stack direction="row">
              <TextField type="password" label="Input your access key" onChange={(event) => {
                setLogSystemKey(event.target.value);
              }} />
              <Button type='button' variant="outlined" onClick={() => {
                setAppliedAccessKey(true);
                StorageHelper.add(ACCESS_KEY_STORAGE, {
                  key: logSystemKey,
                });
              }}>Save</Button>
            </Stack>
          </Box>
        )
      }
      
      {
        isAppliedAccessKey && (
          <LogsOutput logSystemKey={logSystemKey} />
        )
      }
    </Container>
  );
};