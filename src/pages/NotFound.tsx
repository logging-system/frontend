import Container from '@mui/material/Container';
import React from 'react';

export const NotFoundPage: React.FC = () => {

  return (
    <Container maxWidth="lg">
      Not found
    </Container>
  );
};