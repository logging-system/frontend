import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import * as React from 'react';
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
import { createBrowserRouter,RouterProvider } from 'react-router-dom';

import { MainPage } from './layout/main';
import { store } from './store/Store';

const root = createRoot(document.body.querySelector('#root')!);

const router = createBrowserRouter([
  {
    path:    '*',
    element: <MainPage />,
  },
]);

root.render(
  <React.StrictMode>
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Provider store={store}>
        <RouterProvider router={router} />
      </Provider>
    </LocalizationProvider>
  </React.StrictMode>
);