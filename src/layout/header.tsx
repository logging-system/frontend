import HomeIcon from '@mui/icons-material/Home';
import MenuIcon from '@mui/icons-material/Menu';
import { ListItemButton } from '@mui/material';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import React, { useState } from 'react';

export const Header: React.FC = () => {
  const [isDrawerOpened, setDrawerOpen] = useState(false);

  return (
    <Toolbar>
      <IconButton edge="start" color="inherit" aria-label="menu" onClick={() => {
        setDrawerOpen(!isDrawerOpened);
      }}>
        <MenuIcon />
        <Drawer
          anchor="left"
          open={isDrawerOpened}
          onClose={() => {
            setDrawerOpen(false);
          }}
        >
          <List>
            <ListItemButton
              onClick={() => {
                window.location.href = '/';
              }}
            >
              <ListItemIcon>
                <HomeIcon />
              </ListItemIcon>
              <ListItemText>Home</ListItemText>
            </ListItemButton>
          </List>
        </Drawer>
      </IconButton>
      <Typography component="div" variant="h6">Dastanaron-log-system</Typography>
      <Divider  style={{
        margin: 'auto'
      }} />
      <Divider   style={{
        margin: 'auto'
      }}  />
    </Toolbar>
  );
};
