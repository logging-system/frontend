import './styles/layout.scss';

import MuiAlert, { AlertProps } from '@mui/material/Alert';
import AppBar from '@mui/material/AppBar';
import Snackbar from '@mui/material/Snackbar';
import React from 'react';
import { Route, Routes } from 'react-router-dom';

import { useAppDispatch,useAppSelector } from '../hooks/ReduxHooks';
import { HomePage } from '../pages/Home';
import { NotFoundPage } from '../pages/NotFound';
import { setSnackMessage } from '../store/NotificationReducer';
import { Header } from './header';

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref,
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const DEFAULT_TIMER_NOTIFICATION = 3000;

export const MainPage: React.FC = () => {
  const appDispatch = useAppDispatch();
  const snackbarMessage = useAppSelector((state) => state.notification.snackBarMessage);
  const snackbarType = useAppSelector((state) => state.notification.snackMessageType);
  return (
    <React.Fragment>
      <AppBar position="static">
        <Header />
      </AppBar>
      <Snackbar
        anchorOrigin={{
          vertical:   'top',
          horizontal: 'right'
        }}
        autoHideDuration={DEFAULT_TIMER_NOTIFICATION}
        onClose={() => {
          appDispatch(setSnackMessage({ message: '', type: 'info' }));
        }}
        open={snackbarMessage !== ''}
      >
        <Alert onClose={() => {
          appDispatch(setSnackMessage({ message: '', type: 'info' }));
        }} severity={snackbarType} sx={{ width: '100%' }}>
          {snackbarMessage}
        </Alert>
      </Snackbar>
      <div
        style={{
          marginTop: '1rem',
        }}
      >
        { 
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="*" element={<NotFoundPage />} />
          </Routes>
        }
      </div>
    </React.Fragment>
  );
};
