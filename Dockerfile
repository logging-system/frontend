FROM node:18.15

RUN mkdir -p /app

WORKDIR /app

COPY . .

RUN yarn && yarn build
